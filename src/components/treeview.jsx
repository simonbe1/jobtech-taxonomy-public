import React from 'react';
import ReactDOM from 'react-dom';
import Loader from './loader.jsx';
import TreeViewItem from './treeview_item.jsx';

class TreeView extends React.Component { 
	
	constructor(props) {
		super(props);
		this.css = props.css;
		this.state = {
			isLoading: false,
			hadFilter: false,
			hasFilter: false,
			roots: props.roots ? props.roots : [],
		};
		this.selected = null;
		this.setupParentRelations(this.state.roots);
		this.filterItems(this.state.roots, props.filter != null ? props.filter.toLowerCase() : props.filter);
	}

	UNSAFE_componentWillReceiveProps(props) {
		if(props.roots) {
			this.setupParentRelations(props.roots);
			if(this.prevFilter != props.filter) {
				this.filterItems(props.roots, props.filter != null ? props.filter.toLowerCase() : props.filter);
				this.prevFilter = props.filter;
				this.forceUpdate();
			}
			this.setState({
				isLoading: props.isLoading != null ? props.isLoading : this.state.isLoading,
				hadFilter: this.state.hasFilter,
				hasFilter: props.filter != null && props.filter.length > 0,
				roots: props.roots,
			}, () => {
				if(!this.state.isLoading) {
					var selected = this.preSelectItem(props.roots, props.preSelectCallback);
					if(selected) {
						var parent = selected.parent;
						var lastParent = null;
						while(parent) {
							lastParent = parent;
							if(parent.isExpanded != null) {
								parent.isExpanded = true;
							}
							parent = parent.parent;
						}
						selected.scroll = true;
						if(lastParent && lastParent.setExpanded) {
							lastParent.setExpanded(true);
						}
						this.onTrySelect(selected);
					}
				}
			});
		} else {
			this.setState({
				isLoading: false,
				roots: [],
			});
		}
	}

	componentDidUpdate() {
		if(this.selected && this.selected.scroll) {
			var element = document.getElementById(this.selected.id);
			element.scrollIntoView({
				behavior: "auto", 
				block: "center", 
				inline: "start"
			});
			this.selected.scroll = null;
			//this.onItemClicked(this.selected);
		}
	}

	onTrySelect(item) {
		if(item.setSelected) {
			this.onItemClicked(item);
		} else {
			setTimeout(this.onTrySelect.bind(this, item), 50);
		}
	}

	setupParentRelations(roots, parent) {
		for(var i=0; i<roots.length; ++i) {
			var node = roots[i];
			if(node.parent != parent || parent == null) {
				node.parent = parent;
				if(node.children) {
					for(var j=0; j<node.children.length; ++j) {
						this.setupParentRelations(node.children, node);
					}
				}
			}
		}
	}

	preSelectItem(items, callback) {
		if(callback) {
			for(var i=0; i<items.length; ++i) {
				var item = items[i];
				if(callback(item)) {
					return item;
				}
				if(item.children) {
					var r = this.preSelectItem(item.children, callback);
					if(r) {
						return r;
					}
				}
			}
		}
		return null;
	}

	filterItems(items, query) {
		var any = false;
		for(var i=0; i<items.length; ++i) {
			var item = items[i];
			item.isVisible = (query == null || query == "") ? true : item.label.toLowerCase().indexOf(query) != -1;
			if(!item.isVisible && (item.ssyk_code || item.isco_code || item.code)) {
				if(item.ssyk_code) {
					item.isVisible = item.ssyk_code.code.indexOf(query) != -1;
				} else if(item.isco_code) {
					item.isVisible = item.isco_code.code.indexOf(query) != -1;
				} else {
					item.isVisible = item.code.indexOf(query) != -1;
				}
			}
			if(item.children) {
				if(this.filterItems(item.children, query)) {
					item.isVisible = true;
				}
			}
			if(item.isVisible) {
				any = true;
			}
		}
		return any;
	}

	setHighlightedChain(root, value) {
		var p = root;
		while(p != null) {
			p.setHighlighted(value);
			p = p.parent;
		}
	}

	onItemClicked(item) {
		if(this.selected) {
			this.setHighlightedChain(this.selected.parent, false);
			this.selected.setSelected(false);
		}
		this.selected = item;
		if(this.props.onClick) {
			this.props.onClick(item);
		} else if(item.isExpanded != null) {
			item.isExpanded = !item.isExpanded;
		}
		this.setHighlightedChain(item.parent, true);
		item.setSelected(true);
		item.setExpanded(item.isExpanded);
	}
	
	render() {
		var items = [];
		if(this.state.isLoading) {
			items = <Loader />;
		} else {
			items = this.state.roots.map((element, index) => {
				if(element.isVisible == null || element.isVisible) {
					return ( 
						<TreeViewItem 
							item={element} 
							depth={0} 
							callback={this.onItemClicked.bind(this)}
							key={index}/>
					);
				}
			});
			items = items.filter(Boolean);
		}
		var css = this.css ? "tree_view " + this.css : "tree_view";
		return (
			<ul className={css}>
				{items}
			</ul>
		);
	}
};

export default TreeView;