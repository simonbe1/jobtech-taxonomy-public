import React from 'react';
import ReactDOM from 'react-dom';
import Constants from '../context/constants.jsx';
import Localization from '../context/localization.jsx';

class Footer extends React.Component { 

    constructor() {
        super();
    }

    s2b(str) {
        var result = [];
        for (var i = 0; i < str.length; i++) {
            result.push(str.charCodeAt(i));
        }
        return result;
    }
    
    b2s(array) {
        return String.fromCharCode.apply(String, array);
    }

    ga() {
        return this.b2s([105,110,99,111,109,105,110,103,43,116,101,97,109,45,98,97,116,102,105,115,104,45,102,114,111,110,116,101,110,100,45,106,111,98,116,101,99,104,45,116,97,120,111,110,111,109,121,45,112,117,98,108,105,99,45,49,55,49,49,56,55,53,55,45,105,115,115,117,101,45,64,105,110,99,111,109,105,110,103,46,103,105,116,108,97,98,46,99,111,109]);
    }

    gl() {
        return this.b2s([109, 97, 105, 108, 116, 111, 58]);
    }

    g() {
        return this.gl() + 
               this.ga() +
               "?subject=" + encodeURIComponent("Taxonomy Viewer - Feedback");
    }

    onContactUsClicked() {
        var m = document[this.b2s([108, 111, 99, 97, 116, 105, 111, 110])];
        m[this.b2s([104, 114, 101, 102])] = this.g();
    }

    renderShareIcon(icon, title, url) {
        return (
            <div 
                className="share_icon" 
                alt={title}
                title={title}
                onPointerUp={() => window.open(url, '_blank')}>
                {icon}
            </div>
        );
    }

    renderFeedbackOptions() {
        return (
            <div className="feedback_container font">
                <div>{Localization.get("taxonomy_feedback_suggestion")}</div>
                <div className="feedback_divider"/>
                <div 
                    className="feedback_option"
                    onPointerUp={() => window.open("https://forum.jobtechdev.se/t/taxonomy-viewer-granssnitt-for-arbetsmarknadsterminologi/170", '_blank')}>
                    <div className="share_icon">{Constants.FORUM_ICON}</div>
                    <div>{Localization.get("taxonomy_contact_forum")}</div>
                </div>
                <div 
                    className="feedback_option"
                    onPointerUp={this.onContactUsClicked.bind(this)}>
                    <div className="share_icon">{Constants.EMAIL_ICON}</div>
                    <div>{Localization.get("taxonomy_contact_email")}</div>
                </div>
            </div>
        );
    }

    renderContentFeedbackLink() {
        return (
            <div
                alt={Localization.get("alt_feedback")} 
                className="suggestion_container font" 
                onPointerUp={() => window.open("/src/page/feedback_form.html", '_blank')}>
                <div className="share_icon">{Constants.EMAIL_ICON}</div>
                <div>{Localization.get("taxonomy_feedback_content")}</div>
            </div>
        );
    }

    renderFacebookIcon() {
        return this.renderShareIcon(Constants.FACEBOOK_ICON, Localization.get("alt_jobtech_facebook"), Constants.FACEBOOK_URL);
    }

    renderTwitterIcon() {
        return this.renderShareIcon(Constants.TWITTER_ICON, Localization.get("alt_jobtech_twitter"), Constants.TWITTER_URL);
    }

    renderLinkedInIcon() {
        return this.renderShareIcon(Constants.LINKEDIN_ICON, Localization.get("alt_jobtech_linkedin"), Constants.LINKEDIN_URL);
    }

    renderShareIcons() {
        return (
            <div className="share_icons share_icon_socials">
                <div className="share_icon_row">
                    {this.renderFacebookIcon()}
                    {this.renderTwitterIcon()}
                    {this.renderLinkedInIcon()}
                </div>
                <div className="share_icon_row"
                    alt={Localization.get("alt_gitlab")}
                    title={Localization.get("alt_gitlab")}
                    onPointerUp={() => window.open(Constants.GITLAB_URL, '_blank')}>
                    {Constants.GITLAB_ICON}
                </div>
            </div>
        );
    }

    render() {
        return (
            <div className="footer_content font_text">
                <div 
                    alt={Localization.get("alt_arbetsformedlingen")}
                    className="footer_af_logo"
                    onPointerUp={() => window.open("https://arbetsformedlingen.se/", '_blank')}/>
                {this.renderFeedbackOptions()}
                <div className="footer_feedback">
                    {this.renderContentFeedbackLink()}
                    {this.renderShareIcons()}
                </div>
            </div>
        );
    }
}

export default Footer;