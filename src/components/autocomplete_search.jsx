import React from 'react';
import ReactDOM from 'react-dom';
import Support from './../support.jsx';
import Loader from './../components/loader.jsx';
import Rest from './../context/rest.jsx';
import Constants from './../context/constants.jsx';
import Localization from './../context/localization.jsx';

class AutocompleteSearch extends React.Component { 

	constructor() {
        super();
        Support.init();
        this.state = {
            query: "",
            selected: null,
            autocomplete: [],
            isLoadingQuery: false,
            isLoadingContent: false,
        };
    }

    onQueryChanged(e) {
        if(e.target.value.length > 0) {
            Rest.abort();
            Rest.getAutocompleteConcepts(e.target.value, 5, "ssyk-level-4", (data) => {
                this.setState({
                    autocomplete: data,
                    isLoadingQuery: false,
                });
            }, () => {
                this.setState({isLoadingQuery: false});
            });
            this.setState({
                query: e.target.value,
                isLoadingQuery: true,
            });
        } else {
            Rest.abort();
            this.setState({
                query: e.target.value,
                autocomplete: [],
                isLoadingQuery: false,
            });
        }
    }

    onSuggestionClicked(item) {
        this.setState({
            isLoadingContent: true,
            query: "",  
            autocomplete: [],
        });
        Rest.getConcept(item.id, null, (data) => {
            if(this.props.callback) {
                this.props.callback(data[0]);
            }
            this.setState({isLoadingContent: false});
        }, () => {
            this.setState({isLoadingContent: false});
        });
    }

    renderSuggestions() {
        if(this.state.autocomplete.length) {
            return (
                <ul className="autocomplete">
                    {this.state.autocomplete.map((element, index) => {
                        return (
                            <li 
                                key={index}
                                onMouseUp={this.onSuggestionClicked.bind(this, element)}>
                                {element.preferredLabel} ({Localization.get("db_" + element.type)})
                            </li>
                        );
                    })}
                </ul>
            );
        } else if(this.state.isLoadingQuery) {
            return (
                <div className="autocomplete">
                    <Loader />
                </div>
            );
        }
    }

    render() {
        return (
            <div className="autocomplete_input_field">
                <input 
                    type="text"
					placeholder={this.props.placeholder}
                    value={this.state.query}
                    onChange={this.onQueryChanged.bind(this)}/>
                {this.renderSuggestions()}
            </div>
        );
    }
	
}

export default AutocompleteSearch;