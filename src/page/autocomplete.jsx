import React from 'react';
import ReactDOM from 'react-dom';
import Support from './../support.jsx';
import Header from './../components/header.jsx';
import Footer from './../components/footer.jsx';
import Loader from './../components/loader.jsx';
import Rest from './../context/rest.jsx';
import Constants from './../context/constants.jsx';
import Localization from './../context/localization.jsx';

class Autocomplete extends React.Component { 

	constructor() {
        super();
        Support.init();
        this.state = {
            query: "",
            selected: null,
            autocomplete: [],
            isLoadingQuery: false,
            isLoadingContent: false,
        };
    }
    
    componentDidMount() {
        EventDispatcher.add(this.forceUpdate.bind(this), Constants.EVENT_LANGUAGE_CHANGED);
    }

    onQueryChanged(e) {
        if(e.target.value.length > 0) {
            Rest.abort();
            Rest.getAutocompleteConcepts(e.target.value, 5, "ssyk-level-4", (data) => {
                this.setState({
                    autocomplete: data,
                    isLoadingQuery: false,
                });
            }, () => {
                this.setState({isLoadingQuery: false});
            });
            this.setState({
                query: e.target.value,
                isLoadingQuery: true,
            });
        } else {
            Rest.abort();
            this.setState({
                query: e.target.value,
                autocomplete: [],
                isLoadingQuery: false,
            });
        }
    }

    onSuggestionClicked(item) {
        this.setState({
            query: "",  
            autocomplete: [],
        });
        Rest.getConcept(item.id, null, (data) => {
            this.setState({
                selected: data[0],
                isLoadingContent: false,
            });
        }, () => {
            this.setState({isLoadingContent: false});
        });
        this.setState({isLoadingContent: true});
    }

    renderSuggestions() {
        if(this.state.autocomplete.length) {
            return (
                <div className="autocomplete">
                    {this.state.autocomplete.map((element, index) => {
                        return (
                            <div 
                                key={index}
                                onMouseUp={this.onSuggestionClicked.bind(this, element)}>
                                {element.preferredLabel}
                            </div>
                        );
                    })}
                </div>
            );
        } else if(this.state.isLoadingQuery) {
            return (
                <div className="autocomplete">
                    <Loader />
                </div>
            );
        }
    }

    renderInputField() {
        return (
            <div className="input_field">
                <input 
                    type="text"
                    value={this.state.query}
                    onChange={this.onQueryChanged.bind(this)}/>
                {this.renderSuggestions()}
            </div>
        );
    }

    renderSelected() {
        if(this.state.selected) {
            var item = this.state.selected;
            return (
                <div className="selected_container font_text">
                    <div className="selected_title">
                        {item.preferredLabel}
                    </div>
                    <div>
                        {item.definition}
                    </div>
                </div>
            );
        } else if(this.state.isLoadingContent) {
            return (
                <div className="selected_container font_text">
                    <Loader />
                </div>
            );
        }
    }

    render() {
        return (
            <div className="autocomplete_page">
                <Header/>
                <div className="page_content">
                    <div className="description font_text">
                        <div>{Localization.get("autocomplete_row_1")}</div>
                        <div>{Localization.get("autocomplete_row_2")}</div>
                    </div>
                    <div>
                        {this.renderInputField()}
                        {this.renderSelected()}
                    </div>
                </div>
                <Footer/>
            </div>
        );
    }
	
}

ReactDOM.render(<Autocomplete/>, document.getElementById('content'));