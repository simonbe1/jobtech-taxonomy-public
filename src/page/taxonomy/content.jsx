import React from 'react';
import ReactDOM from 'react-dom';
import TreeView from './../../components/treeview.jsx';
import Rest from './../../context/rest.jsx';
import Util from './../../context/util.jsx';
import Constants from './../../context/constants.jsx';
import Localization from './../../context/localization.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Loader from './../../components/loader.jsx';

class Content extends React.Component { 

	constructor() {
        super();
		// state
        this.state = {
			item: null,
			isFetchingItem: false,
        };
    }

	UNSAFE_componentWillReceiveProps(props) {
        this.setState({
			item: props.item,
			isFetchingItem: props.isFetchingItem,
		});
	}
	
	onSsykPressed(code) {
		this.props.ssykCallback(code);
	}
	
	onIscoPressed(code) {
		this.props.iscoCallback(code);
	}
	
	onFieldPressed(id) {
		this.props.fieldCallback(id);
	}

	onItemClicked(item) {
		if(item.children == null) {
			this.props.otherCallback(item);
		}
		if(item.isExpanded != null) {
			item.isExpanded = !item.isExpanded;
			this.forceUpdate();
		}
	}

	renderHeader() {
		return (
			<div className="content_header rounded font">
				<h1 className="header_name">
					{this.state.item.label}
				</h1>
			</div>
		);
	}

	renderCode(code, title, key, callback) {
		return (
			<div 
				className="info_code rounded" 
				key={key}
				title={title}
				onPointerUp={callback}>
				{code}
			</div>
		);
	}

	renderInfo() {
		var item = this.state.item;
		var key = 0;
		var codes = [];
		var infoLine = (text, value) => {
			return (
				<div className="info_codes" key={key++}>
					<div>{text}:</div>
					<div className="info_value">{value}</div>
				</div>
			);
		};
		var keyProperties = [
			{ key: "iso_639_3_alpha_2_2007", text: "ISO 639-1 code" },
			{ key: "iso_639_3_alpha_3_2007", text: "ISO 639-2 code" },
			{ key: "nuts_level_3_code_2013", text: "NUTS level 3 code" },
			{ key: "lau_2_code_2015", text: "LAU 2 code / Kommunkod" },
			{ key: "sun_education_field_code_2020", text: "SUN code" },
			{ key: "sun_education_level_code_2020", text: "SUN code" },
		];
		// info
		codes.push(infoLine(Localization.get("concept_type"), Localization.get("db_" + item.type)));
		codes.push(infoLine("Concept-ID", item.id));
		for(var i=0; i<keyProperties.length; ++i) {
			if(item[keyProperties[i].key]) {
				codes.push(infoLine(keyProperties[i].text, item[keyProperties[i].key]));
			}
		}
		// clickable goto links
		if(item.ssyk_code) {
			codes.push(
				<div className="info_codes" key={key++}>
					<div>{Localization.get("ssyk_code")}:</div>
					{this.renderCode(item.ssyk_code.code, item.ssyk_code.label, 0, this.onSsykPressed.bind(this, item.ssyk_code))}
				</div>
			);
		}
		if(item.isco_codes && item.isco_codes.length > 0) {
			codes.push(
				<div className="info_codes" key={key++}>
					<div>{Localization.get("isco_code")}:</div>
					<div>
						{item.isco_codes.map((element, index) => {
							return this.renderCode(element.code, element.label, index, this.onIscoPressed.bind(this, element));
						})}
					</div>
				</div>
			);
		}
		if(item.field) {
			codes.push(
				<div className="info_codes" key={key++}>
					<div>{Localization.get("occupation_field")}:</div>
					{this.renderCode(item.field.label, null, 0, this.onFieldPressed.bind(this, item.field))}
				</div>
			);
		}
		return (
			<div className="content_info">
				<h3>{Localization.get("concept_info")}:</h3>
				{codes}
			</div>
		);
	}

	renderDefinition() {
		return (
			<div className="content_definition">
				<h3>{Localization.get("description")}:</h3>
				<div>{this.state.item.definition}</div>
			</div>
		);
	}

	renderItem() {
		return (
			<div className="content_item font">
				{this.renderDefinition()}
				<div/>
				{this.renderInfo()}
			</div>
		);
	}

	renderRelations() {
		var item = this.state.item;
		var relations = item.relation_list ? item.relation_list.map((element, index) => {
			return (
				<div 
					className="relation_collection"
					key={index}>
					<h2 className="rounded">
						{Localization.get(element.name)}
					</h2>
					<TreeView 
						roots={element.list}
						onClick={this.onItemClicked.bind(this)}/>
				</div>
			);
		}) : [];
		return (
			<div className="content_relations font">
				{relations}
			</div>
		);
	}

    render() {
		if(this.state.isFetchingItem) {
            return (
                <div className="content">
					<Loader/>
				</div>
            );
		} else if(this.state.item) {
            return (
                <div className="content">
                    {this.renderHeader()}
                    {this.renderItem()}
                    {this.renderRelations()}
                </div>
            );
        } else {
            return (
                <div className="content"/>
            );
        }
    }
	
}

export default Content;