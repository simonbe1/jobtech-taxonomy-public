import React from 'react';
import Rest from './../../context/rest.jsx';
import ContextUtil from './../../context/util.jsx';
import Constants from '../../context/constants.jsx';
import Localization from '../../context/localization.jsx';

class Util {

	setupTreeItems(items, sortKey) {
		for(var i=0; i<items.length; ++i) {
			var item = items[i];
			item.isExpanded = false;
			if(item.children) {
				this.setupTreeItems(item.children, sortKey);
				if(item.children.length && item.children[0][sortKey] != null) {
					ContextUtil.sortByKey(item.children, sortKey, true);
				} else {
					ContextUtil.sortByKey(item.children, "label", true);
				}
			}
		}
	}

	setupSsykCode(items) {
		for(var i=0; i<items.length; ++i) {
			var item = items[i];
			if(item.ssyk_code) {
				item.ssyk_code = {
					code: item.ssyk_code,
					label: item.label,
				};
			}
			if(item.children) {
				this.setupSsykCode(item.children);
			}
		}
	}

	setupSubstitues(item) {
		var high = {
			id: "high_substitutability",
			label: Localization.get("high_substitutability"),
			children: []
		};
		var low = {
			id: "low_substitutability",
			label: Localization.get("low_substitutability"),
			children: []
		};
		for(var i=0; i<item.substitutes.length; ++i) {
			var substitute = item.substitutes[i];
			if(substitute.percentage >= 50) {
				high.children.push(substitute);
			} else  {
				low.children.push(substitute);
			}
		}
		//replace current substitutes
		item.substitutes = [];
		if(high.children.length > 0) {
			item.substitutes.push(high);
		}
		if(low.children.length > 0) {
			item.substitutes.push(low);
		}
		this.setupTreeItems(item.substitutes, "label");
	}

	getDisplayTypeForConcept(type) {
		if(type.startsWith("ssyk") || 
		   type == "occupation-name" ||
		   type == "occupation-field") {
			return Constants.DISPLAY_TYPE_WORK_SSYK;
		} else if(type.startsWith("isco")) {
			return Constants.DISPLAY_TYPE_WORK_ISCO;
		} else if(type.startsWith("skill")) {
			return Constants.DISPLAY_TYPE_SKILL;
		} else if(type.startsWith("sun-education")) {
			return Constants.DISPLAY_TYPE_EDUCATION;
		} else if(type.startsWith("employment") ||
				  type == "wage-type" ||
				  type == "worktime-extent" ||
				  type == "occupation-experience-year") {
			return Constants.DISPLAY_TYPE_WORK_DESC;
		} else if(type == "continent" ||
				  type == "country" ||
				  type == "region" ||
				  type == "municipality") {
			return Constants.DISPLAY_TYPE_GEOGRAPHY;
		} else if(type.startsWith("language")) {
			return Constants.DISPLAY_TYPE_LANGUAGE;
		} else if(type.startsWith("sni")) {
			return Constants.DISPLAY_TYPE_INDUSTRY;
		} else if(type.startsWith("keyword")) {
			return Constants.DISPLAY_TYPE_SEARCH;
		}
		return Constants.DISPLAY_TYPE_OTHER;
	}

	processStructure(structure, sortKey) {
		var id = 0;
		var roots = [];
		for(var property in structure.data) {
			roots.push({
				id: id++,
				label: Localization.get("db_" + property.replace(/_/g, "-")),
				children: structure.data[property],
			});	
		}
		this.setupTreeItems(roots, sortKey != null ? sortKey : "label");
		ContextUtil.sortByKey(roots, sortKey != null ? sortKey : "label", true);
		return roots;
	}

	conceptQuery(name, query, data) {
		var version = Constants.getArg("v");
		version = version != null ? ", version: \"" + version + "\"" : "";
		var result = (name ? name + ": " : "") + "concepts(" + query + version + ") {";
		if(data) {
			result += data;
		}
		result += "}";
		return result;
	}

	generateQuery() {
		var query = "query MyQuery {";
		for(var i=0; i<arguments.length; ++i) {
			query += arguments[i];
		}
		query += "}";
		return query;
	}

	async getSsykStructureMini() {
		var query = 
			this.generateQuery(this.conceptQuery(null, "type: \"ssyk-level-1\"",
				Constants.GRAPHQL_SSYK_LIST_TYPE_ITEM +
				"children: narrower(type: \"ssyk-level-2\") {" +
					Constants.GRAPHQL_SSYK_LIST_TYPE_ITEM +
					"children: narrower(type: \"ssyk-level-3\") {" +
						Constants.GRAPHQL_SSYK_LIST_TYPE_ITEM +
						"children: narrower(type: \"ssyk-level-4\") {" +
							Constants.GRAPHQL_SSYK_LIST_TYPE_ITEM +
						"}" +
					"}" +
				"}"
			)) + Constants.GRAPHQL_SSYK_LIST_TYPE_FRAGMENT;
		var structure = await Rest.getGraphQlPromise(query);
		var data = structure.data.concepts;
		this.setupTreeItems(data, "ssyk_code");
		ContextUtil.sortByKey(data, "ssyk_code", true);
		this.setupSsykCode(data);
		return data;
	}

	async getSsykStructure() {
		var query = 
			this.generateQuery(this.conceptQuery(null, "type: \"ssyk-level-1\"",
				Constants.GRAPHQL_SSYK_LIST_TYPE_ITEM +
				"children: narrower(type: \"ssyk-level-2\") {" +
					Constants.GRAPHQL_SSYK_LIST_TYPE_ITEM +
					"children: narrower(type: \"ssyk-level-3\") {" +
						Constants.GRAPHQL_SSYK_LIST_TYPE_ITEM +
						"children: narrower(type: \"ssyk-level-4\") {" +
							Constants.GRAPHQL_SSYK_LIST_TYPE_ITEM +
							"children: narrower(type: \"occupation-name\") {" +
								Constants.GRAPHQL_DEFAULT_ITEM +
							"}" +
						"}" +
					"}" +
				"}"
			)) + Constants.GRAPHQL_SSYK_LIST_TYPE_FRAGMENT;
		var structure = await Rest.getGraphQlPromise(query);
		this.setupTreeItems(structure.data.concepts, "ssyk_code");
		ContextUtil.sortByKey(structure.data.concepts, "ssyk_code", true);
		this.setupSsykCode(structure.data.concepts);
		return structure.data.concepts;
	}
	
	async getIscoStructure() {
		var query = 
			this.generateQuery(this.conceptQuery(null, "type: \"isco-level-4\"",
				Constants.GRAPHQL_DEFAULT_ITEM +
				"isco_code: isco_code_08 "
			));
		var structure = await Rest.getGraphQlPromise(query);
		ContextUtil.sortByKey(structure.data.concepts, "isco_code", true);
		for(var i=0; i<structure.data.concepts.length; ++i) {
			var item = structure.data.concepts[i];
			item.isco_code = {
				code: item.isco_code,
				label: item.label,
			};
		}
		return structure.data.concepts;
	}

	async getSsykGroups() {
		var query = 
			this.generateQuery(this.conceptQuery(null, "type: \"ssyk-level-4\"",
				Constants.GRAPHQL_SSYK_LIST_TYPE_ITEM +
				"children: narrower(type: \"occupation-name\") {" +
					Constants.GRAPHQL_DEFAULT_ITEM +
				"}"
			)) + Constants.GRAPHQL_SSYK_LIST_TYPE_FRAGMENT;
		var structure = await Rest.getGraphQlPromise(query);
		this.setupTreeItems(structure.data.concepts, "label");
		ContextUtil.sortByKey(structure.data.concepts, "ssyk_code", true);
		this.setupSsykCode(structure.data.concepts);
		return structure.data.concepts;
	}

	async getOccupations() {
		var query = 
			this.generateQuery(this.conceptQuery(null, "type: \"occupation-name\"", 
				Constants.GRAPHQL_DEFAULT_ITEM
			));
		var structure = await Rest.getGraphQlPromise(query);
		ContextUtil.sortByKey(structure.data.concepts, "label", true);
		return structure.data.concepts;
	}

	async getFields() {
		var query = 
			this.generateQuery(this.conceptQuery(null, "type: \"occupation-field\"",
				Constants.GRAPHQL_DEFAULT_ITEM +
				"children: narrower(type: \"ssyk-level-4\") {" +
					Constants.GRAPHQL_SSYK_LIST_TYPE_ITEM +
					"children: narrower(type: \"occupation-name\") {" +
						Constants.GRAPHQL_DEFAULT_ITEM +
					"}" +
				"}"
			)) + Constants.GRAPHQL_SSYK_LIST_TYPE_FRAGMENT;
		var structure = await Rest.getGraphQlPromise(query);
		this.setupTreeItems(structure.data.concepts, "ssyk_code");
		ContextUtil.sortByKey(structure.data.concepts, "label", true);
		this.setupSsykCode(structure.data.concepts);
		return structure.data.concepts;
	}

	async getSkills() {
		var query = 
			this.generateQuery(this.conceptQuery(null, "type: \"skill-headline\"",
				Constants.GRAPHQL_DEFAULT_ITEM +
				"children: narrower(type: \"skill\") {" +
					Constants.GRAPHQL_DEFAULT_ITEM +
				"}"
			));
		var structure = await Rest.getGraphQlPromise(query);
		this.setupTreeItems(structure.data.concepts, "label");
		ContextUtil.sortByKey(structure.data.concepts, "label", true);
		return structure.data.concepts;
	}

	async getIndustry() {
		var query = 
			this.generateQuery(this.conceptQuery("sni", "type: \"sni-level-1\"",
				Constants.GRAPHQL_DEFAULT_ITEM + 
				"children: narrower(type: \"sni-level-2\") {" +
					Constants.GRAPHQL_DEFAULT_ITEM + 
				"}"
			));
		var structure = await Rest.getGraphQlPromise(query);
		return this.processStructure(structure);
	}

	async getKeywords() {
		var query = 
			this.generateQuery(this.conceptQuery("keyword", "type: \"keyword\"",
				Constants.GRAPHQL_DEFAULT_ITEM + 
				"related(type: \"occupation-name\") {" +
					Constants.GRAPHQL_DEFAULT_ITEM + 
				"}"
			));
		var structure = await Rest.getGraphQlPromise(query);
		var items = this.processStructure(structure);
		for(var i=0; i<items[0].children.length; ++i) {
			var item = items[0].children[i];
			if(item.relation_list == null) {
				item.relation_list = [];
			}
			if(item.related.length) {
				this.setupSsykCode(item.related);
				item.relation_list.push({
					name: "taxonomy_item_related_occupations",
					list: item.related,
				});
			}
		}
		return items;
	}

	async getLanguage() {
		var query = 
		this.generateQuery(
			this.conceptQuery("language", "type: \"language\"",
				Constants.GRAPHQL_DEFAULT_ITEM + 
				"iso_639_3_alpha_2_2007 " +
				"iso_639_3_alpha_3_2007 "
			),
			this.conceptQuery("language_level", "type: \"language-level\"",
				Constants.GRAPHQL_DEFAULT_ITEM
			)
		);
		var structure = await Rest.getGraphQlPromise(query);
		return this.processStructure(structure);
	}

	async getEducation() {
		var query = 
			this.generateQuery(
				this.conceptQuery("sun_education_field", "type: \"sun-education-field-1\"",
					Constants.GRAPHQL_DEFAULT_ITEM + 
					"sun_education_field_code_2020 " +
					"code: sun_education_field_code_2020 " +
					"children: narrower(type: \"sun-education-field-2\") {" +
						Constants.GRAPHQL_DEFAULT_ITEM + 
						"sun_education_field_code_2020 " +
						"code: sun_education_field_code_2020 " +
						"children: narrower(type: \"sun-education-field-3\") {" +
							Constants.GRAPHQL_DEFAULT_ITEM + 
							"sun_education_field_code_2020 " +
							"code: sun_education_field_code_2020 " +
							"children: narrower(type: \"sun-education-field-4\") {" +
								Constants.GRAPHQL_DEFAULT_ITEM + 
								"sun_education_field_code_2020 " +
								"code: sun_education_field_code_2020 " +
							"}" +
						"}" +
					"}"
				),
				this.conceptQuery("sun_education_level", "type: \"sun-education-level-1\"",
					Constants.GRAPHQL_DEFAULT_ITEM + 
					"sun_education_level_code_2020 " +
					"code: sun_education_level_code_2020 " +
					"children: narrower(type: \"sun-education-level-2\") {" +
						Constants.GRAPHQL_DEFAULT_ITEM + 
						"sun_education_level_code_2020 " +
						"code: sun_education_level_code_2020 " +
						"children: narrower(type: \"sun-education-level-3\") {" +
							Constants.GRAPHQL_DEFAULT_ITEM + 
							"sun_education_level_code_2020 " +
							"code: sun_education_level_code_2020 " +
						"}" +
					"}"
				)
			);
		var structure = await Rest.getGraphQlPromise(query);
		return this.processStructure(structure, "code");
	}

	async getGeography() {
		var query = 
			this.generateQuery(this.conceptQuery("continent", "type: \"continent\"",
				Constants.GRAPHQL_DEFAULT_ITEM + 
				"children: narrower(type: \"country\") {" + 
					Constants.GRAPHQL_DEFAULT_ITEM + 
					"children: narrower(type: \"region\") {" + 
						Constants.GRAPHQL_DEFAULT_ITEM + 
						"nuts_level_3_code_2013 " +
						"children: narrower(type: \"municipality\") {" + 
							Constants.GRAPHQL_DEFAULT_ITEM + 
							"lau_2_code_2015 " +
						"}" +
					"}" +
				"}"
			));
		var structure = await Rest.getGraphQlPromise(query);
		return this.processStructure(structure);
	}

	async getOccupationDesc() {
		var query = 
			this.generateQuery(
				this.conceptQuery("employment_duration", "type: \"employment-duration\"", Constants.GRAPHQL_DEFAULT_ITEM),
				this.conceptQuery("employment_type", "type: \"employment-type\"", Constants.GRAPHQL_DEFAULT_ITEM),
				this.conceptQuery("wage_type", "type: \"wage-type\"", Constants.GRAPHQL_DEFAULT_ITEM),
				this.conceptQuery("worktime_extent", "type: \"worktime-extent\"", Constants.GRAPHQL_DEFAULT_ITEM),
				this.conceptQuery("occupation_experience_year", "type: \"occupation-experience-year\"", Constants.GRAPHQL_DEFAULT_ITEM),
			);
		var structure = await Rest.getGraphQlPromise(query);
		return this.processStructure(structure);
	}

	async getOthers() {
		var query = 
			this.generateQuery(this.conceptQuery("driving_licence", "type: \"driving-licence\"", Constants.GRAPHQL_DEFAULT_ITEM));
		var structure = await Rest.getGraphQlPromise(query);
		return this.processStructure(structure);
	}

	async getConceptBase(id) {
		var query = 
			this.generateQuery(this.conceptQuery(null, "id: \"" + id + "\"",
				"definition " +
				"type " +
				"label: preferred_label " +
				"substitutes(type: \"occupation-name\") {" +
					"id " +
					"type " +
					"label: preferred_label " +
					"percentage: substitutability_percentage " +
				"} " +
				"keywords: related(type: \"keyword\") {" +
					"id " +
					"type " +
					"label: preferred_label " +
				"} " +
				"isco_codes: broader(type: \"isco-level-4\") {" +
					"code: isco_code_08 " +
					"label: preferred_label " +
				"} "
			));
		var structure = await Rest.getGraphQlPromise(query);
		var data = structure.data.concepts[0];
		if(data.substitutes) {
			this.setupSubstitues(data);
			ContextUtil.sortByKey(data.substitutes, "label", true);
		}
		if(data.keywords) {
			ContextUtil.sortByKey(data.keywords, "label", true);
		}
		return data;
	}

    async setupSsykItem(item) {
		var query = 
			this.generateQuery(this.conceptQuery(null, "id: \"" + item.id + "\"",
				"definition " +
				"ssyk_code: ssyk_code_2012 " +
				"field: broader(type: \"occupation-field\") {" +
					"id " +
					"label: preferred_label " +
				"} " +
				"skills: related(type: \"skill\") {" +
					"id " +
					"label: preferred_label " +
					"headline: broader(type: \"skill-headline\") {" +
						"id " +
						"label: preferred_label " +
					"} " +
				"} " +
				"isco_codes: related(type: \"isco-level-4\") {" +
					"code: isco_code_08 " +
					"label: preferred_label " +
				"} "
			));
		var structure = await Rest.getGraphQlPromise(query);
		var data = structure.data.concepts[0];
		// setup skill tree
		if(data.skills) {
			var headlines = [];
			for(var i=0; i<data.skills.length; ++i) {
				var skill = data.skills[i];
				var headline = skill.headline[0];
				var current = headlines.find((x) => {
					return x.id == headline.id;
				});
				if(current == null) {
					current = {
						id: headline.id,
						label: headline.label,
						type: "skill-headline",
						isExpanded: false,
						children: [],
					};
					headlines.push(current);
				}
				current.children.push({
					id: skill.id,
					label: skill.label,
					type: "skill",
				});
				ContextUtil.sortByKey(current.children, "label", true);
			}
			ContextUtil.sortByKey(headlines, "label", true);
			data.skills = headlines;
		}
		// setup field
		if(data.field) {
			data.field = data.field[0];
		}
		// finalize item
		if(item.ssyk_code == null) {
			item.ssyk_code = {
				code: data.ssyk_code,
				label: item.label,
			};
		}
		item.definition = data.definition;
		item.field = data.field;
		item.isco_codes = data.isco_codes;
		item.relation_list = [{
			name: "taxonomy_item_skills",
			list: data.skills,
		}];
		item.isLoaded = true;
	}

    async setupIscoItem(item) {
		var query = 
			this.generateQuery(this.conceptQuery(null, "id: \"" + item.id + "\"",
				"definition " +
				"isco_code: isco_code_08 " +
				"ssyk_codes: related(type: \"ssyk-level-4\") {" +
					"code: ssyk_code_2012 " +
					"label: preferred_label " +
				"} "
			));
		var structure = await Rest.getGraphQlPromise(query);
		var data = structure.data.concepts[0];
		// finalize item
		if(item.isco_code == null) {
			item.isco_code = {
				code: data.isco_code,
				label: item.label,
			};
		}
		if(data.ssyk_codes && data.ssyk_codes.length > 0) {
			item.ssyk_code = data.ssyk_codes[0];
		}
		item.definition = data.definition;
		item.isco_codes = [item.isco_code];
	}

	async setupSkillItem(item) {
		var query = 
			this.generateQuery(this.conceptQuery(null, "id: \"" + item.id + "\"",
				"definition " +
				"occupations: related(type: \"ssyk-level-4\") {" +
					"id " +
					"type " +
					"label: preferred_label " +
					"ssyk_code: ssyk_code_2012 " +
				"} "
			));
		var structure = await Rest.getGraphQlPromise(query);
		var data = structure.data.concepts[0];
		item.definition = data.definition;
		if(item.relation_list == null) {
			item.relation_list = [];
		}
		if(data.occupations.length) {
			this.setupSsykCode(data.occupations);
			item.relation_list.push({
				name: "taxonomy_item_skill_relations",
				list: data.occupations,
			});
		}
	}
	
	async getSsykParent(child) {
		var query = 
			this.generateQuery(this.conceptQuery(null, "id: \"" + child.id + "\"",
				"parent: broader(type: \"ssyk-level-4\") {" +
					"id " +
					"type " +
					"label: preferred_label " +
					"ssyk_code: ssyk_code_2012 " +
				"} "
			));
		var structure = await Rest.getGraphQlPromise(query);
		console.log(structure);
		var data = structure.data.concepts[0];
		// setup parent
		if(data.parent) {
			data.parent = data.parent[0];
			if(data.parent.ssyk_code) {
				data.parent.ssyk_code = {
					code: data.parent.ssyk_code,
					label: data.parent.label,
				};
			}
		}
		child.parent = data.parent;
	}

	async setupItem(item) {
		if(item.isLoaded) {
			// item is already loaded
			return;
		}
        if(item.type.startsWith("ssyk")) {
			await this.setupSsykItem(item);
        } else if(item.type.startsWith("isco")) {
			await this.setupIscoItem(item);
        } else if(item.type.startsWith("skill")) {
			await this.setupSkillItem(item);
        } else {
			if(item.parent == null && item.type == "occupation-name") {
				await this.getSsykParent(item);
			}
            // TODO: fetch item data in a more specific way?
			var concept = await this.getConceptBase(item.id);
			item.definition = concept.definition;
			item.label = concept.label;
			if(item.relation_list == null) {
				item.relation_list = [];
			}
			if(concept.substitutes && concept.substitutes.length) {
				item.relation_list.push({
					name: "taxonomy_item_substitues",
					list: concept.substitutes,
				});
			}
			if(concept.keywords && concept.keywords.length) {
				item.relation_list.push({
					name: "taxonomy_item_keywords",
					list: concept.keywords,
				});
			}
			var parent = item.parent;
            if(parent && parent.type && parent.type.startsWith("ssyk")) {
				// TODO: setup parent for all types?
				await this.setupItem(parent);
				item.field = parent.field;
				item.ssyk_code = parent.ssyk_code;
				if(item.type == "occupation-name") {
					item.isco_codes = concept.isco_codes;
				} else {
					item.isco_codes = parent.isco_codes;
				}
				// TODO: dont merge all lists?
                item.relation_list = parent.relation_list.concat(item.relation_list);
			}
        }
		item.isLoaded = true;
	}
	
}

export default new Util;