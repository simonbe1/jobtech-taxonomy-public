import React from 'react';
import ReactDOM from 'react-dom';
import Support from './../support.jsx';
import Header from './../components/header.jsx';
import Footer from './../components/footer.jsx';
import Loader from './../components/loader.jsx';
import TreeView from './../components/treeview.jsx';
import Constants from './../context/constants.jsx';
import Search from './taxonomy/search.jsx';
import Content from './taxonomy/content.jsx';
import GotoDialog from './taxonomy/goto_dialog.jsx';
import Util from './taxonomy/util.jsx';
import EventDispatcher from './../context/event_dispatcher.jsx';
import Localization from './../context/localization.jsx';
import Rest from './../context/rest.jsx';

class Taxonomy extends React.Component { 

	constructor() {
        super();
		Support.init();
		// constants
		this.SHOW_STRUCTURE = 0;
		this.SHOW_GROUPS = 1;
		this.SHOW_OCCUPATIONS = 2;
		this.SHOW_FIELDS = 3;
		this.SHOW_ISCO = 4;
		this.SHOW_SKILLS = 5;
		this.SHOW_OTHER = 6;
        this.SHOW_WORK_DESC = 7;
        this.SHOW_GEOGRAPHY = 8;
        this.SHOW_INDUSTRY = 9;
        this.SHOW_SEARCH = 10;
        this.SHOW_LANGUAGE = 11;
        this.SHOW_EDUCATION = 12;
		// state
        this.state = {
			items: [],
			selected: null,
			filter: "",
			isLoading: true,
			isFetchingItem: false,
			dialog: null,
			popupText: null,
			showType: -1,
		};
		this.preSelectType = null;
	}
	
	componentDidMount() {
        EventDispatcher.add(this.forceUpdate.bind(this), Constants.EVENT_LANGUAGE_CHANGED);
        EventDispatcher.add(this.onShowPopup.bind(this), Constants.EVENT_SHOW_POPUP_INDICATOR);
        EventDispatcher.add(this.onHidePopup.bind(this), Constants.EVENT_HIDE_POPUP_INDICATOR);
		// check if url points to a targeted concept
		var hashConcept = Constants.getArg("concept");
		if(hashConcept && hashConcept.length > 3) {
			var version = Constants.getArg("v");
			Rest.getConcept(hashConcept, version, (data) => {
				if(data.length > 0) {
					var type = Util.getDisplayTypeForConcept(data[0].type);
					this.onDisplayTypeChanged(type, {
						key: "id",
						value: data[0].id,
					});
					EventDispatcher.fire(Constants.EVENT_TAXONOMY_SET_SEARCH_TYPE, {
						type: type,
						radio: Constants.DISPLAY_WORK_TYPE_GROUP
					});
				}
			});
		} else {
			this.showGroups();
		}
	}

	async setTreeViewData(type, method) {
		if(this.state.showType != type) {
			this.setState({isLoading: true});
			var data = await method();
			this.setState({
				showType: type,
				items: data,
				isLoading: false,
			});
		}
	}

	async showStructure() {
		await this.setTreeViewData(this.SHOW_STRUCTURE, () => { return Util.getSsykStructure(); });
	}

	async showGroups() {
		await this.setTreeViewData(this.SHOW_GROUPS, () => { return Util.getSsykGroups(); });
	}

	async showOccupations() {
		await this.setTreeViewData(this.SHOW_OCCUPATIONS, () => { return Util.getOccupations(); });
	}

	async showFields() {
		await this.setTreeViewData(this.SHOW_FIELDS, () => { return Util.getFields(); });
	}

	async showIsco() {
		await this.setTreeViewData(this.SHOW_ISCO, () => { return Util.getIscoStructure(); });
	}

	async showSkills() {
		await this.setTreeViewData(this.SHOW_SKILLS, () => { return Util.getSkills(); });
	}

	async showOccupationDesc() {
		await this.setTreeViewData(this.SHOW_WORK_DESC, () => { return Util.getOccupationDesc(); });
	}

	async showGeography() {
		await this.setTreeViewData(this.SHOW_GEOGRAPHY, () => { return Util.getGeography(); });
	}

	async showIndustry() {
		await this.setTreeViewData(this.SHOW_INDUSTRY, () => { return Util.getIndustry(); });
	}

	async showKeyword() {
		await this.setTreeViewData(this.SHOW_SEARCH, () => { return Util.getKeywords(); });
	}

	async showLanguage() {
		await this.setTreeViewData(this.SHOW_LANGUAGE, () => { return Util.getLanguage(); });
	}

	async showEducation() {
		await this.setTreeViewData(this.SHOW_EDUCATION, () => { return Util.getEducation(); });
	}

	async showOther() {
		await this.setTreeViewData(this.SHOW_OTHER, () => { return Util.getOthers(); });
	}

	async fetchItem(item) {
		this.setState({isFetchingItem: true});
		await Util.setupItem(item);
		this.setState({
			selected: item,
			isFetchingItem: false,
		});
	}

	onDisplayTypeChanged(type, preSelectType) {
		this.preSelectType = preSelectType;
		if(type == Constants.DISPLAY_TYPE_WORK_SSYK) {
			this.showGroups();
		} else if(type == Constants.DISPLAY_TYPE_WORK_ISCO) {
			this.showIsco();
		} else if(type == Constants.DISPLAY_TYPE_SKILL) {
			this.showSkills();
		} else if(type == Constants.DISPLAY_TYPE_WORK_DESC) {
			this.showOccupationDesc();
		} else if(type == Constants.DISPLAY_TYPE_GEOGRAPHY) {
			this.showGeography();
		} else if(type == Constants.DISPLAY_TYPE_INDUSTRY) {
			this.showIndustry();
		} else if(type == Constants.DISPLAY_TYPE_SEARCH) {
			this.showKeyword();
		} else if(type == Constants.DISPLAY_TYPE_LANGUAGE) {
			this.showLanguage()
		} else if(type == Constants.DISPLAY_TYPE_EDUCATION) {
			this.showEducation()
		} else if(type == Constants.DISPLAY_TYPE_OTHER) {
			this.showOther();
		}
	}

	onDisplayWorkTypeChanged(type) {
		this.preSelectType = null;
		if(type == Constants.DISPLAY_WORK_TYPE_STRUCTURE) {
			this.showStructure();
		} else if(type == Constants.DISPLAY_WORK_TYPE_GROUP) {
			this.showGroups();
		} else if(type == Constants.DISPLAY_WORK_TYPE_OCCUPATIONS) {
			this.showOccupations();
		} else if(type == Constants.DISPLAY_WORK_TYPE_FIELDS) {
			this.showFields();
		}
	}

	onFilterChanged(filter) {
		this.preSelectType = null;
		this.setState({filter: filter});
	}

	onItemClicked(item) {
		this.preSelectType = null;
		if(item != this.state.selected && item.type) {
			this.fetchItem(item);
		}
		if(item.isExpanded != null) {
			item.isExpanded = !item.isExpanded;
		}
		Constants.replaceArg("concept", item.id);
	}

	onAutocompleteItemClicked(item) {
		// TODO: populate special fields
		item.label = item.preferredLabel;
		this.onOtherPressed(item, true);
	}

	showGotoDialog(text, callback, skipDialog) {
		var yesCallback = () => {
			callback();
			this.setState({
				dialog: null,
				filter: "",
			});
		};
		if(skipDialog) {
			yesCallback();
		} else {
			var dialog = 
				<GotoDialog 
					text={text}
					yesCallback={yesCallback}
					noCallback={() => this.setState({dialog: null})}/>;
			this.setState({dialog: dialog});
		}
	}
	
	// the following 3 callbacks are called from the special tags in the info block
	onSsykPressed(ssyk) {
		this.showGotoDialog(Localization.get("db_ssyk-level-4") + ", " + ssyk.label, () => {
			this.switchAndSelect(Constants.DISPLAY_TYPE_WORK_SSYK, false, "ssyk_code", ssyk.code, this.showStructure.bind(this));
		});
	}
	
	onIscoPressed(isco) {
		this.showGotoDialog(Localization.get("db_isco-level-4") + ", " + isco.label, () => {
			this.switchAndSelect(Constants.DISPLAY_TYPE_WORK_ISCO, false, "isco_code", isco.code, this.showIsco.bind(this));
		});
	}
	
	onFieldPressed(field) {
		this.showGotoDialog(Localization.get("db_occupation-field") + ", " + field.label, () => {
			this.switchAndSelect(Constants.DISPLAY_TYPE_WORK_SSYK, true, "id", field.id, this.showFields.bind(this));
		});
	}

	// this callback is called from relation connections
	onOtherPressed(item, skipDialog) {
		if(item.type == "skill") {
			this.showGotoDialog(Localization.get("db_skill") + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_SKILL, true, "id", item.id, this.showSkills.bind(this));
			}, skipDialog);
		} else if(item.type == "ssyk-level-4") {
			this.showGotoDialog(Localization.get("db_ssyk-level-4") + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_WORK_SSYK, false, "id", item.id, this.showGroups.bind(this));
			}, skipDialog);
		} else if(item.type == "isco-level-4") {
			this.showGotoDialog(Localization.get("db_isco-level-4") + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_WORK_ISCO, false, "id", item.id, this.showIsco.bind(this));
			}, skipDialog);
		} else if(item.type == "occupation-field") {
			this.showGotoDialog(Localization.get("db_occupation-field") + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_WORK_SSYK, true, "id", item.id, this.showFields.bind(this));
			}, skipDialog);
		} else {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.preSelectType = {
					key: "id",
					value: item.id,
				};
				this.fetchItem(item);
			}, skipDialog);
		}
	}

    onShowPopup(text) {
        var indicator = document.getElementById("save_indicator");
        indicator.classList.add("save_enter_margin");
        this.setState({popupText: text});
    }

    onHidePopup() {
        var indicator = document.getElementById("save_indicator");
        indicator.classList.remove("save_enter_margin");
    }

	switchAndSelect(type, isFields, key, value, showMethod) {
		EventDispatcher.fire(Constants.EVENT_TAXONOMY_SET_SEARCH_TYPE, {
			type: type,
			radio: isFields ? Constants.DISPLAY_WORK_TYPE_FIELDS : Constants.DISPLAY_WORK_TYPE_GROUP
		});
		this.preSelectType = {
			key: key,
			value: value,
		};
		this.setState({isFetchingItem: true});
		showMethod();
	}

	onPreSelect(item) {
		if(this.preSelectType) {
			if(this.preSelectType.key == "ssyk_code") {
				return item.ssyk_code && item.ssyk_code.code == this.preSelectType.value;
			} else if(this.preSelectType.key == "isco_code") {
				return item.isco_code && item.isco_code.code == this.preSelectType.value;
			}
			return item[this.preSelectType.key] == this.preSelectType.value;
		}
		return false;
	}

	renderTreeView() {
		return ( 
			<div className="search_panel">
				<Search 
					onDisplayTypeChanged={this.onDisplayTypeChanged.bind(this)}
					onDisplayWorkTypeChanged={this.onDisplayWorkTypeChanged.bind(this)}
					onFilterChanged={this.onFilterChanged.bind(this)}
					onAutocompleteItemClicked={this.onAutocompleteItemClicked.bind(this)}
					filter={this.state.filter}/>
				<TreeView 
					preSelectCallback={this.onPreSelect.bind(this)}
					isLoading={this.state.isLoading}
					filter={this.state.filter}
					roots={this.state.items}
					onClick={this.onItemClicked.bind(this)}/> 
			</div>
		);
	}

	renderDialog() {
		if(this.state.dialog) {
			return this.state.dialog;
		}
	}
	
    renderSaveIndicator() {
        return (
            <div className="save_indicator_content">
                <div
                    id="save_indicator" 
                    className="save_indicator font">
                    <div className="loader"/>
                    <div>
                        {this.state.popupText}
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return (
            <div className="taxonomy_page">
                <Header/>
				<div className="page_content">
					{this.renderTreeView()}
					<Content 
						ssykCallback={this.onSsykPressed.bind(this)}
						iscoCallback={this.onIscoPressed.bind(this)}
						fieldCallback={this.onFieldPressed.bind(this)}
						otherCallback={this.onOtherPressed.bind(this)}
						isFetchingItem={this.state.isFetchingItem}
						item={this.state.selected}/>
					{this.renderDialog()}
				</div>
				{this.renderSaveIndicator()}
                <Footer/>
            </div>
        );
    }
	
}

ReactDOM.render(<Taxonomy/>, document.getElementById('content'));