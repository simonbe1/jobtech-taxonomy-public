import React from 'react';
import ReactDOM from 'react-dom';
import Rest from './../../context/rest.jsx';
import Constants from './../../context/constants.jsx';
import Localization from './../../context/localization.jsx';
import Util from './util.jsx';

class OtherForm extends React.Component { 

	constructor() {
        super();
        this.state = {
            value: "",
        };
    }

    onValueChanged(value, e) {
        var state = {};
        state[value] = e.target.value;
        this.setState(state);
    }

    onSendPressed() {
        var subject = "Taxonomy Viewer - Förslag " + Localization.get("feedback_something_else");
        var message = Localization.get("feedback_other_text") + "\n" + this.state.value ;
        Util.send(subject, message);
    }

    renderMulti(text, value) {
        return (
            <div className="form_field font">
                <label for="other">
                    {Localization.get(text)}
                </label>
                <textarea 
                    className="rounded"
                    id="other"
                    type="text"
                    value={this.state[value]}
                    onChange={this.onValueChanged.bind(this, value)}/>
            </div>
        );
    }

    renderButton() {
        return (
            <div 
                className="form_send_button font"
                onPointerUp={this.onSendPressed.bind(this)}>
                {Localization.get("send")}
            </div>
        );
    }

    render() {
        return (
            <div className="form_container">
                {this.renderMulti("feedback_other_text", "value")}
                {this.renderButton()}
            </div>
        );
    }
	
}

export default OtherForm;