
import Constants from './constants.jsx';

class Localization { 

    constructor() {
        // localization
        this.resources = {
            // swedish
            sv: {
                locale: "sv-SE",
                /* db types */
                db_continent: "Världsdel",
                db_country: "Land",
                "db_driving-licence": "Körkort",
                "db_driving-licence-combination": "Körkorts-kombination",
                "db_employment-duration": "Anställningsvaraktighet",
                "db_employment-type": "Anställningsform",
                "db_isco-level-1": "ISCO nivå 1",
                "db_isco-level-4": "ISCO nivå 4",
                db_keyword: "Sökbegrepp",
                db_language: "Språk",
                "db_language-level": "Språknivå",
                db_municipality: "Kommun",
                "db_occupation-collection": "Yrkessamling",
                "db_occupation-field": "Yrkesområde",
                "db_occupation-name": "Yrkesbenämning",
                "db_occupation-experience-year": "Tid i yrke",
                db_region: "EU-Region",
                db_skill: "Kompetensbegrepp",
                "db_skill-headline": "Kompetensbegrepp",
                "db_sni": "SNI",
                "db_sni-level-1": "SNI nivå 1",
                "db_sni-level-2": "SNI nivå 2",
                "db_ssyk": "SSYK",
                "db_ssyk-level-1": "SSYK nivå 1",
                "db_ssyk-level-2": "SSYK nivå 2",
                "db_ssyk-level-3": "SSYK nivå 3",
                "db_ssyk-level-4": "SSYK nivå 4",
                "db_sun-education-field": "Utbildningsriktning",
                "db_sun-education-field-1": "Utbildningsinriktning nivå 1",
                "db_sun-education-field-2": "Utbildningsinriktning nivå 2",
                "db_sun-education-field-3": "Utbildningsinriktning nivå 3",
                "db_sun-education-field-4": "Utbildningsinriktning nivå 4",
                "db_sun-education-level": "Utbildningsnivå",
                "db_sun-education-level-1": "Utbildningsnivå nivå 1",
                "db_sun-education-level-2": "Utbildningsnivå nivå 2",
                "db_sun-education-level-3": "Utbildningsnivå nivå 3",
                "db_wage-type": "Löneform",
                "db_worktime-extent": "Arbetstid",
                "concept/definition": "Definition",
                "concept/id": "Id",
                "concept/preferredLabel": "Namn",
                "concept/type": "Värdeförråd",
                "concept/sort-order": "Sorteringsordning",
                "concept.external-database.ams-taxonomy-67/id": "Externt id",
                "concept.external-standard/ssyk-code-2012": "SSYK 2012",
                "concept.external-standard/nuts-level-3-code-2013": "NUTS nivå 3",
                "concept.external-standard/isco-code-08": "ISCO 08",
                "concept.external-standard/lau-2-code-2015": "LAU 2 2015",
                "concept.external-standard/driving-licence-code-2013": "Körkortskod 2013", 
                "concept.external-standard/implicit-driving-licences": "Implicit körkort",
                "concept.external-standard/iso-3166-1-alpha-3-2013": "ISO-3166-1 alpha 3",
                "concept.external-standard/iso-3166-1-alpha-2-2013": "ISO-3166-1 alpha 2",
                "concept.external-standard/sni-level-code-2007": "SNI nivå 2007",
                "concept.external-standard/iso-639-3-alpha-2-2007": "ISO-639-3 alpha 2",
                "concept.external-standard/iso-639-3-alpha-3-2007": "ISO-639-3 alpha 3",
                "concept.external-standard/sun-education-field-code-2020": "Utbildningsinriktning",
                "concept.external-standard/sun-education-level-code-2020": "Utbildningsnivå",                
                /* Actions */
                CREATED: "Skapad",
                UPDATED: "Uppdaterad",
                DEPRECATED: "Avaktualiserad",
                COMMENTED: "Anteckning",
                /* regular words */          
                "occupation-name": "Yrkesbenämning",
                "occupation-field": "Yrkesområde",
                node_view: "Nod-vy",
                node_description: "Utforska taxonomin med en dynamisk nod-vy",
                node_indepth_description: "Den interaktiva grafen nedan ger en inblick i det komplexa nätverk som taxonomi-strukturen består av.",
                node_tip_1: "- Klicka på en vitt nod för att expandera den och se dess kopplingar",
                node_tip_2: "- Klicka och drag noder",
                node_tip_3: "- Zooma in och ut",
                autocomplete: "Automatiska sökförslag",
                autocomplete_description: "Sök i taxonomin med hjälp av automatiska sökförslag",
                autocomplete_row_1: "Taxonomy-apiet tillhandahåller en hjälpfunktion för att underlätta sökandet.",
                autocomplete_row_2: "Prova t.ex. att skriva \"adv\" för att se bland annat \"Advokater\".",
                autocomplete_row_3: "Sökningen måste innehålla minst tre tecken.",
                taxonomy: "Taxanomi",
                taxonomy_description: "Taxanomi beskrivning",
                taxonomy_item_definition: "Definition",
                taxonomy_item_skills: "Kompetensbegrepp kopplade till yrkesgruppen",
                taxonomy_item_substitues: "Besläktade yrken",
                taxonomy_item_keywords: "Sökord",
                taxonomy_item_related_occupations: "Besläktade yrken",
                taxonomy_item_skill_relations: "Yrkesgrupper kopplade till kompetensbegreppet",
                taxonomy_feedback_content: "Lämna förslag på innehåll",
                taxonomy_feedback_suggestion: "Vi tar gärna emot dina synpunter om\nTaxonomy Viewer",
                taxonomy_contact_forum: "Kontakta oss via vårt forum",
                taxonomy_contact_email: "Maila till oss",
                ssyk_code: "SSYK-kod",
                isco_code: "ISCO-kod",
                occupations_ssyk: "Yrkesbenämningar (SSYK)",
                occupations_isco: "Yrkesbenämningar (ISCO)",
                skills: "Kompetensbegrepp",
                occupation_structure: "Samtliga SSYK-nivåer",
                occupation_groups: "Yrkesgrupper",
                occupation_only: "Yrkesbenämningar",
                occupation_fields: "Yrkesområden",
                occupation_group: "Yrkesgrupp",
                occupation_field: "Yrkesområde",
                occupation_description: "Arbetsbeskrivningar",
                concept: "Koncept",
                concept_info: "Om begreppet",
                concept_type: "Typ av begrepp",
                description: "Definition",
                autocomplete_placeholder: "Sök...",
                feedback_work_form_name_suggestion: "Förslag på yrkesbenämning:",
                feedback_work_form_task_suggestion: "Vilka arbetsuppgifter är relevanta för yrket:",
                feedback_work_form_skill_suggestion: "Efterfrågad utbilding och/eller kompetens:",
                feedback_work_form_group_suggestion: "Förslag på SSYK-yrkesgrupp som yrkesbenämningen ska kopplas till:",
                feedback_skill_form_name_suggestion: "Förslag på kompetensord:",
                feedback_skill_form_desc_suggestion: "Kort beskrivning av kompetensordet:",
                feedback_skill_form_headline_suggestion: "Förslag på SSYK-yrkesgrupp som yrkesbenämningen ska kopplas till:",
                feedback_other_text: "Beskriv ditt förlsag:",
                feedback_other: "Övrigt",
                feedback_occupation: "Yrkesbenämning",
                feedback_skill: "Kompetensbegrepp",
                feedback_something_else: "Annat",
                show: "Visa",
                link: "Länk",
                send: "Skicka",
                loading: "Laddar",
                explore: "Utforska",
                versions: "Versioner",
                version: "Version",
                export: "Exportera",
                about: "Om",
                yes: "Ja",
                no: "Nej",
                goto: "Vill du gå vidare till",
                saving: "Sparar",
                exporting: "Exporterar",
                other: "Övriga",
                something_else: "Annat",
                geography: "Geografi",
                industry: "Näringsgrenar",
                education: "Utbildning",
                type: "Typ",
                action: "Åtgärd",
                name: "Namn",
                date: "Datum",
                filter: "Filtrera",
                changes: "Ändringar",
                show_info: "Visa info",
                relation_type: "Relationstyp",
                relation: "Relation",
                close: "Stäng",
                id: "Id",
                definition: "Definition",
                preferredLabel: "Namn",
                from: "Från",
                from_id: "Från Id",
                from_type: "Från typ",
                to: "Till",
                to_id: "Till Id",
                to_type: "Till typ",
                action_type: "Typ av händelse",
                version_info_title: "Information om händelse",
                high_substitutability: "Högt släktskap",
                low_substitutability: "Lågt släktskap",
                alt_arbetsformedlingen: "Besök Arbetsförmedlingens hemsida",
                alt_jobtech_facebook: "Besök Jobtechs Facebook",
                alt_jobtech_twitter: "Besök Jobtechs Twitter",
                alt_jobtech_linkedin: "Besök Jobtechs LinkedIn",
                alt_homepage: "Gå till start sidan",
                alt_feedback: "Öppna återkopplings-formulär",
                alt_gitlab: "Besök Taxonomy Viewer Gitlab",
                language: "Språk",
            },
            // english
            en: {
                locale: "en-GB",
                /* db types */
                db_continent: "Continent",
                db_country: "Country",
                "db_driving-licence": "Driving licence",
                "db_driving-licence-combination": "Driving licence combination",
                "db_employment-duration": "Employment duration",
                "db_employment-type": "Employment type",
                "db_isco-level-1": "ISCO level 1",
                "db_isco-level-4": "ISCO level 4",
                db_keyword: "Keyword",
                db_language: "Language",
                "db_language-level": "Language level",
                db_municipality: "Municipality",
                "db_occupation-collection": "Occupation collection",
                "db_occupation-field": "Occuption field",
                "db_occupation-name": "Occupation name",
                "db_occupation-experience-year": "Occupation experience",
                db_region: "Region",
                db_skill: "Skill",
                "db_skill-headline": "Skill headline",
                "db_sni": "SNI",
                "db_sni-level-1": "SNI level 1",
                "db_sni-level-2": "SNI level 2",
                "db_ssyk": "SSYK",
                "db_ssyk-level-1": "SSYK level 1",
                "db_ssyk-level-2": "SSYK level 2",
                "db_ssyk-level-3": "SSYK level 3",
                "db_ssyk-level-4": "SSYK level 4",
                "db_sun-education-field": "Education field",
                "db_sun-education-field-1": "Education field level 1",
                "db_sun-education-field-2": "Education field level 2",
                "db_sun-education-field-3": "Education field level 3",
                "db_sun-education-field-4": "Education field level 4",
                "db_sun-education-level": "Education level",
                "db_sun-education-level-1": "Education level 1",
                "db_sun-education-level-2": "Education level 2",
                "db_sun-education-level-3": "Education level 3",
                "db_wage-type": "Wage type",
                "db_worktime-extent": "Worktime extent",
                "concept/definition": "Definition",
                "concept/id": "Id",
                "concept/preferredLabel": "Name",
                "concept/type": "Type",
                "concept/sort-order": "Sort order",
                "concept.external-database.ams-taxonomy-67/id": "External id",
                "concept.external-standard/ssyk-code-2012": "SSYK 2012",
                "concept.external-standard/nuts-level-3-code-2013": "NUTS level 3",
                "concept.external-standard/isco-code-08": "ISCO 08",
                "concept.external-standard/lau-2-code-2015": "LAU 2 2015",
                "concept.external-standard/driving-licence-code-2013": "Driving licence code 2013", 
                "concept.external-standard/implicit-driving-licences": "Implicit driving licence",
                "concept.external-standard/iso-3166-1-alpha-3-2013": "ISO-3166-1 alpha 3",
                "concept.external-standard/iso-3166-1-alpha-2-2013": "ISO-3166-1 alpha 2",
                "concept.external-standard/sni-level-code-2007": "SNI level 2007",
                "concept.external-standard/iso-639-3-alpha-2-2007": "ISO-639-3 alpha 2",
                "concept.external-standard/iso-639-3-alpha-3-2007": "ISO-639-3 alpha 3",
                "concept.external-standard/sun-education-field-code-2020": "Education field code",
                "concept.external-standard/sun-education-level-code-2020": "Education level code",                
                /* Actions */
                CREATED: "Created",
                UPDATED: "Updated",
                DEPRECATED: "Deprecated",
                COMMENTED: "Commented",
                /* regular words */          
                "occupation-name": "Occupation name",
                "occupation-field": "Occupation field",
                node_view: "Node view",
                node_description: "Explore the taxonomy with the dynamic node view",
                node_indepth_description: "The interactive graph view below gives a small glimps into the complex structure of the taxonomy-data.",
                node_tip_1: "- Click a white node to expand it and see its connection",
                node_tip_2: "- Click and drag nodes",
                node_tip_3: "- Zoom in and out",
                autocomplete: "Autocomplete",
                autocomplete_description: "Search the taxonomy-data with the help of autocomplete",
                autocomplete_row_1: "The taxonomy-api provides an autocomplete function, allowing users to easily find what they are looking for.",
                autocomplete_row_2: "Try typing \"adv\" to see \"Advokater\" among others.",
                taxonomy: "Taxonomy",
                taxonomy_description: "Taxonomy description",
                taxonomy_item_definition: "Definition",
                taxonomy_item_skills: "Skills connected to occupation",
                taxonomy_item_substitues: "Related occupations",
                taxonomy_item_keywords: "Keywords",
                taxonomy_item_related_occupations: "Related occupations",
                taxonomy_item_skill_relations: "Occupation groups connected to skill",
                taxonomy_feedback_content: "Suggest content",
                taxonomy_feedback_suggestion: "We would like your feedback on\nTaxonomy Viewer",
                taxonomy_contact_forum: "Visit our forum",
                taxonomy_contact_email: "Contact us by mail",
                ssyk_code: "SSYK-code",
                isco_code: "ISCO-code",
                occupations_ssyk: "Occupations (SSYK)",
                occupations_isco: "Occupations (ISCO)",
                skills: "Skills",
                occupation_structure: "Occupation structure",
                occupation_groups: "Occupation groups",
                occupation_only: "Only occupations",
                occupation_fields: "Occupation fields",
                occupation_group: "Occupation group",
                occupation_field: "Occupation field",
                concept: "Concept",
                concept_info: "Information about the concept",
                concept_type: "Type of concept",
                occupation_description: "Occupation descriptions",
                description: "Description",
                autocomplete_placeholder: "Search the entire taxonomy...",
                feedback_work_form_name_suggestion: "Occupation suggestion:",
                feedback_work_form_task_suggestion: "Tasks relevant for the occupation:",
                feedback_work_form_skill_suggestion: "Required education and/or skill:",
                feedback_work_form_group_suggestion: "Suggested SSYK-group to connect the occupation to:",
                feedback_skill_form_name_suggestion: "Skill-name suggestion:",
                feedback_skill_form_desc_suggestion: "Short description about the skill:",
                feedback_skill_form_headline_suggestion: "Suggested skill-headline to connect the new skill to:",
                feedback_other_text: "Describe your suggestion:",
                feedback_other: "Other:",
                feedback_occupation: "Occupation",
                feedback_skill: "Skill",
                feedback_something_else: "Other",
                show: "Show",
                link: "Link",
                send: "Send",
                loading: "Loading",
                explore: "Explore",
                versions: "Versions",
                version: "Version",
                export: "Export",
                about: "About",
                saving: "Saving",
                yes: "Yes",
                no: "No",
                goto: "Would you like to proceed to",
                exporting: "Exporting",
                other: "Other",
                geography: "Geography",
                industry: "Industry",
                education: "Utbildning",
                something_else: "Other",
                type: "Type",
                action: "Action",
                name: "Name",
                date: "Date",
                changes: "Changes",
                filter: "Filter",
                show_info: "Show info",
                relation_type: "Relation type",
                relation: "Relation",
                close: "Close",
                id: "Id",
                definition: "Definition",
                preferredLabel: "Name",
                from: "From",
                from_id: "From Id",
                from_type: "From type",
                to: "To",
                to_id: "To Id",
                to_type: "To type",
                action_type: "Type of action",
                version_info_title: "Information about change",
                high_substitutability: "High substitutability",
                low_substitutability: "Low substitutability",
                alt_arbetsformedlingen: "Visit Arbetsförmedlingens homepage",
                alt_jobtech_facebook: "Visit Jobtechs Facebook",
                alt_jobtech_twitter: "Visit Jobtechs Twitter",
                alt_jobtech_linkedin: "Visit Jobtechs LinkedIn",
                alt_homepage: "Go to homepage",
                alt_feedback: "Open feedback form",
                alt_gitlab: "Visit Taxonomy Viewer Gitlab",
                language: "Language",
            }
        }
        // find active language
        this.code = this.setLanguage();
    }
	
    setLanguage() {
        var lang = navigator.language || navigator.userLanguage;
        var langSimple = lang.split("-")[0];
        var key = null;
        console.log("local language: " + lang);
        if(Constants.lang) {
            console.log("override language: " + Constants.lang);
            lang = Constants.lang;
        } else {
            var prefLang = localStorage.getItem("taxonomy_lang");
            if(prefLang != null) {
                lang = prefLang;
            }
        }
        if(this.resources[lang] != null) {
            key = lang;
        } else if(this.resources[langSimple] != null) {
            key = langSimple;
        }
        if(key != null) {
            console.log("selecting localization: " + key);
            this.lang = this.resources[key];
            this.key = key;
        } else {
            console.log("localization not found: '" + key + "' defaulting to english");
            this.lang = this.resources["en"];
            this.key = "en";
        }
        return lang;
    }

    changeLanguage(code) {
        var key = null;
        if(this.resources[code] != null) {
            key = code;
        }
        if(key != null) {
            console.log("changing localization: " + key);
            this.lang = this.resources[key];
            this.code = "sv-SE";
            this.key = key;
        } else {
            console.log("localization not found: '" + key + "' defaulting to english");
            this.lang = this.resources["en"];
            this.code = "en-GB";
            this.key = "en";
        }
        localStorage.setItem("taxonomy_lang", this.key);
    }

    get(key) {
        if(this.lang[key] == null) {
            var en = this.resources["en"];
            if(en == null || en[key] == null) {
                return key;
            }
            return en[key];
        }
        return this.lang[key];
    }

    getUpper(key) {
        return this.lang[key].toUpperCase();
    }
    
    getLower(key) {
        return this.lang[key].toLowerCase();
    }
}

export default new Localization;