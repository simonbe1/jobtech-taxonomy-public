FROM node as builder

COPY package*  webpack* /app/
COPY src/  /app/src/
COPY config.js  /app/config.js
COPY build_finalizer.js  /app/build_finalizer.js
#COPY . /app/
WORKDIR /app

# this part just makes sure there arent any compile errors
RUN npm install
RUN npm run release



#FROM nginx
FROM nginx
COPY --from=builder /app/target/ /usr/share/nginx/html/
COPY --from=builder /app/target/index.html  /usr/share/nginx/html/index.html
COPY default.conf /etc/nginx/conf.d
COPY nginx.conf /etc/nginx/
COPY config.js  /mnt/volumemount/config.js
RUN ln -s /mnt/volumemount/config.js /usr/share/nginx/html/config.js

RUN chgrp -R 0 /etc/nginx  && chmod -R g=u /etc/nginx
RUN chgrp -R 0 /var/cache/nginx  && chmod -R g=u /var/cache/nginx
RUN chgrp -R 0 /var/log/nginx  && chmod -R g=u /var/log/nginx
#RUN chgrp -R 0 /var/run && chmod -R g=u /var/run

#EXPOSE 8000
#CMD npm run  build
