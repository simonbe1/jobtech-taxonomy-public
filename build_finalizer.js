var fs = require('fs');

var errorLog = function(v) {
	if(v) {
		console.log(v);
	}
};

var deleteFolderRecursive = function(path) {
	if( fs.existsSync(path) ) {
		fs.readdirSync(path).forEach(function(file,index){
			var curPath = path + "/" + file;
			if(fs.lstatSync(curPath).isDirectory()) { // recurse
				deleteFolderRecursive(curPath);
			} else { // delete file
				fs.unlinkSync(curPath);
			}
		});
		fs.rmdirSync(path);
	}
};

var copyFolderRecursive = function(path, destination) {
	if( fs.existsSync(path) ) {
		fs.readdirSync(path).forEach(function(file,index){
			var curPath = path + "/" + file;
			var dstPath = destination + "/" + file;
			if(fs.lstatSync(curPath).isDirectory()) { // recurse			
				fs.mkdirSync(dstPath, errorLog);
				copyFolderRecursive(curPath, dstPath);
			} else { // copy file
				fs.copyFile(curPath, dstPath, errorLog);
			}
		});
	}
};

var replaceHtmlContent = function(htmlFile, files) {
	fs.readFile(htmlFile, 'utf8', function (err, data) {
		if (err) {
			return console.log(err);
		}
		var result = data;		
		for(var i=0; i<files.length; ++i) {
			if(files[i].replace) {
				result = result.replace(files[i].src, files[i].dst);
			}
		}
		fs.writeFile(htmlFile, result, 'utf8', errorLog);
	});
};

try {
	var startTime = Date.now();
	console.log('\nSetting up release package...');
	
	var time = Date.now();
	var srcRoot = './src/';
	var dstRoot = './target/';
	var htmlFileDsts = [];
	htmlFileDsts.push(dstRoot + 'index.html');
	htmlFileDsts.push(dstRoot + 'page/autocomplete.html');
	htmlFileDsts.push(dstRoot + 'page/node.html');
	htmlFileDsts.push(dstRoot + 'page/taxonomy.html');
	htmlFileDsts.push(dstRoot + 'page/feedback_form.html');
	htmlFileDsts.push(dstRoot + 'page/version.html');
	var files = [
		{ src: 'index.html', dst: 'index.html' },
		{ src: 'page/autocomplete.html', dst: 'page/autocomplete.html' },
		{ src: 'page/node.html', dst: 'page/node.html' },
		{ src: 'page/taxonomy.html', dst: 'page/taxonomy.html' },
		{ src: 'page/feedback_form.html', dst: 'page/feedback_form.html' },
		{ src: 'page/version.html', dst: 'page/version.html' },
		{ src: 'style/app.css', dst: 'style/app_' + time + '_.css', replace: true },
		{ src: 'style/autocomplete.css', dst: 'style/autocomplete_' + time + '_.css', replace: true },
		{ src: 'style/index.css', dst: 'style/index_' + time + '_.css', replace: true },
		{ src: 'style/node.css', dst: 'style/node_' + time + '_.css', replace: true },
		{ src: 'style/taxonomy.css', dst: 'style/taxonomy_' + time + '_.css', replace: true },
		{ src: 'style/feedback_form.css', dst: 'style/feedback_form_' + time + '_.css', replace: true },
		{ src: 'style/version.css', dst: 'style/version_' + time + '_.css', replace: true },
		{ src: 'js/autocomplete.js', dst: 'js/autocomplete_' + time + '_.js', replace: true },
		{ src: 'js/index.js', dst: 'js/index_' + time + '_.js', replace: true },
		{ src: 'js/node.js', dst: 'js/node_' + time + '_.js', replace: true },
		{ src: 'js/taxonomy.js', dst: 'js/taxonomy_' + time + '_.js', replace: true },
		{ src: 'js/feedback_form.js', dst: 'js/feedback_form_' + time + '_.js', replace: true },
		{ src: 'js/version.js', dst: 'js/version_' + time + '_.js', replace: true },
		{ src: 'js/autocomplete.js.LICENSE.txt', dst: 'js/autocomplete_' + time + '_.js.LICENSE.txt', replace: true },
		{ src: 'js/index.js.LICENSE.txt', dst: 'js/index_' + time + '_.js.LICENSE.txt', replace: true },
		{ src: 'js/node.js.LICENSE.txt', dst: 'js/node_' + time + '_.js.LICENSE.txt', replace: true },
		{ src: 'js/taxonomy.js.LICENSE.txt', dst: 'js/taxonomy_' + time + '_.js.LICENSE.txt', replace: true },
		{ src: 'js/feedback_form.js.LICENSE.txt', dst: 'js/feedback_form_' + time + '_.js.LICENSE.txt', replace: true },
		{ src: 'js/version.js.LICENSE.txt', dst: 'js/version_' + time + '_.js.LICENSE.txt', replace: true },
	];
	
	// remove release package to ensure unwanted files are removed
	console.log('Removing old target folder...');
	deleteFolderRecursive('./target');
	
	// setup release package
	console.log('Setting up new target...');
	fs.mkdirSync('./target', errorLog);
	fs.mkdirSync('./target/js', errorLog);
	fs.mkdirSync('./target/page', errorLog);
	fs.mkdirSync('./target/resources', errorLog);
	fs.mkdirSync('./target/style', errorLog);
	
	// copy files
	console.log('Copying files...');
	for(var i=0; i<files.length; ++i) {
		fs.copyFile(srcRoot + files[i].src, dstRoot + files[i].dst, errorLog);
	}
	copyFolderRecursive('./src/resources', './target/resources');
	
	// append name replacements
	files.push( {src: './../config.js', dst: './config.js', replace: true} );
	files.push( {src: './../../config.js', dst: './../config.js', replace: true} );
	
	// rename files in html file
	console.log('Finalizing files...');
	for(var i=0; i<htmlFileDsts.length; ++i) {
		replaceHtmlContent(htmlFileDsts[i], files);
	}
	
	// upate js file
	replaceHtmlContent(dstRoot + 'js/index_' + time + '_.js', [{src: 'window.location.pathname="/src/page/taxonomy.html"', dst: 'window.location.pathname="/page/taxonomy.html"', replace: true}]);
	
	console.log('-----------------------');
	console.log('Build complete!');
	console.log('Finalizing took: ' + (Date.now() - startTime) + " ms");
} catch(e) {
	console.log(e);
}
